/*******************************************************************************
** 文件名称：gm_multi_key_cfg.h
** 文件作用：多功能按键管理配置文件
** 编写作者：Tom Free 付瑞彪
** 编写时间：2020-12-28
** 文件备注：配置按键管理模块的参数，方便移植
** 更新记录：
**          2020-12-28 -> 创建文件
**                                                            <Tom Free 付瑞彪>
**
**              Copyright (c) 2018-2020 付瑞彪 All Rights Reserved
**
**       1 Tab == 4 Spaces     UTF-8     ANSI C Language(C99)
*******************************************************************************/
#ifndef __GM_MULTI_KEY_CFG_H__
#define __GM_MULTI_KEY_CFG_H__

/* 按键轮询时间间隔，用于系统的相关时间计算，单位:ms */
#define GM_MULTI_KEY_POLL_INTERVAL      (10)

/* 默认的消抖时间滴答数，以上面的间隔为基准 */
#define GM_MULTI_KEY_DEBOUNCE_TICKS     (2)

/* 默认的连击最长间隔时间滴答数，超过此时间认为连击被终结，换算成滴答数，以上面的间隔为基准 */
#define GM_MULTI_KEY_HIT_AGIN_TICKS     (30)

/* 默认的长按触发时间，超过此时间认为进入长按，换算成滴答数，以上面的间隔为基准 */
#define GM_MULTI_KEY_LONG_TICKS         (100)

/* 默认的长按重复触发时间，即隔多长时间触发一次，换算成滴答数，以上面的间隔为基准 */
#define GM_MULTI_KEY_REPEAT_TRIG_TICKS  (10)

/* 默认的连击最大次数，注意不要超过数据最大范围（8位无符号） */
#define GM_MULTI_KEY_CLICK_COUNT_MAX    (200)

#endif	/* __GM_MULTI_KEY_CFG_H__ */
